class ArticlesController < ApplicationController
  before_action :set_article, only: [:edit, :update]
	def index
  	@articles = Article.all 
  end

	def new
		@article = Article.new
	end

	def create
		@article = Article.create(article_params)
  end

  def edit
  end

  def update
    @article.update_attributes(article_params)
	end

  def show
  	@article = Article.find(params[:id])
  end

	def destroy
  	@article = Article.find(params[:id])
  	@article.destroy
 		redirect_to articles_path
	end
	
  private

  def article_params
    params.require(:article).permit(:title, :text)
  end

  def set_article
    @article  = Article.find(params[:id])
  end

end
